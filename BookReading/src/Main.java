import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int q = scanner.nextInt();

        for (int i = 0; i < q; i++) {
            long n = scanner.nextLong();
            long m = scanner.nextLong();


            long index;
            long p;

            long d = 0;
            for (int j = 0; j < n; j++) {
                if (m % 10 == 0)
                    index = 0;
                else if (m % 5 == 0)
                    index = 25;
                else if (m % 2 == 0)
                    index = 40;
                else
                    index = 45;

                p = n / (m * 10);
                d = p * index;
                for (long k = (p * m * 10) + m; k <= n; k += m)
                    d += k % 10;
            }
            System.out.println(d);
        }

    }
}
