import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int first[] = new int[n];
        int second[] = new int[n-1];
        int third[] = new int[n-2];
        ArrayList<Integer> res = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            first[i] = scanner.nextInt();
        }
        Arrays.sort(first);

        for (int i = 0; i < n - 1 ; i++) {
            second[i] = scanner.nextInt();
        }
        Arrays.sort(second);

        for (int i = 0; i < n - 2 ; i++) {
            third[i] = scanner.nextInt();
        }
        Arrays.sort(third);


        int counter1;
        for (int i = 0; i < first.length; i++) {
            counter1 = 0;
            for (int j = 0; j < second.length; j++) {
                if(first[i] == second[j]){
                   counter1++;
                }
            }
            if(counter1 == 0){
               res.add(first[i]);
            }
        }

        int counter2;
        for (int i = 0; i < second.length; i++) {
            counter2 = 0;
            for (int j = 0; j < third.length; j++) {
                if(second[i] == third[j]){
                    counter2++;
                }
            }
            if(counter2 == 0){
               res.add(second[i]);
            }
        }

        for (Integer r : res) {
            System.out.println(r);
        }
    }
}
