import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int array[] = new int[2*n];

        for (int i = 0; i < 2*n; i++) {
            array[i] = scanner.nextInt();
        }

        Arrays.sort(array);

        int sum1 = 0;
        int sum2 = 0;

        for (int i = 0; i < n; i++) {
            sum1 += array[i];
        }

        for (int i = n; i < 2*n; i++) {
            sum2 += array[i];
        }

        if(sum1 == sum2){
            System.out.println("-1");
        }else {
            for (int i : array) {
                System.out.print(i+" ");
            }
        }
    }
}
