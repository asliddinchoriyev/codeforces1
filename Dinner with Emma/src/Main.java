import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int min = 0;
        int max = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int a = scanner.nextInt();
                if (j == 0 || min > a) {
                    min = a;
                }
            }
            if (max < min) {
                max = min;
            }
        }
        System.out.println(max);
    }
}