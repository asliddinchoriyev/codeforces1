import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String result = "";

        String input = scanner.nextLine();
        String zero = scanner.nextLine();
        String one = scanner.nextLine();
        String two = scanner.nextLine();
        String three = scanner.nextLine();
        String four = scanner.nextLine();
        String five = scanner.nextLine();
        String six = scanner.nextLine();
        String seven = scanner.nextLine();
        String eight = scanner.nextLine();
        String nine = scanner.nextLine();

        for (int i = 0; i < 80; i += 10) {
           String target = input.substring(i,i+10);

           if(target.equals(zero)){
               result += 0;
           }

            if(target.equals(one)){
                result += 1;
            }

            if(target.equals(two)){
                result += 2;
            }

            if(target.equals(three)){
                result += 3;
            }

            if(target.equals(four)){
                result += 4;
            }

            if(target.equals(five)){
                result += 5;
            }

            if(target.equals(six)){
                result += 6;
            }

            if(target.equals(seven)){
                result += 7;
            }

            if(target.equals(eight)){
                result += 8;
            }

            if(target.equals(nine)){
                result += 9;
            }
        }

        System.out.println(result);

    }
}
