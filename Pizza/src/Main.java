import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        long numberOfFriends = scanner.nextLong();

        long result;

        if (numberOfFriends == 0){
            result = 0;
        }
        else if ((numberOfFriends + 1) % 2 == 1) {
            result = numberOfFriends + 1;
        } else {
            result = (numberOfFriends + 1) / 2;
        }

        System.out.println(result);
    }


}
