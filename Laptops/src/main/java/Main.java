import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean ans = false;

        for (int i = 0; i < n; i++) {
            int price = scanner.nextInt();
            int quality = scanner.nextInt();

            if (price != quality) {
                ans = true;
                break;
            }
        }
        if (ans) {
            System.out.println("Happy Alex");
        } else {
            System.out.println("Poor Alex");
        }
    }
}
