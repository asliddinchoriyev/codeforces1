import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int div,rem;

        int counter = a;
        while (a >= b) {

            div = a / b;
            counter += div;

            rem = a % b;
            a = div + rem;
        }

        System.out.println(counter);

    }
}
