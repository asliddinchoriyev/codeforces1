import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        boolean helper = true;

        int counter = 0;
        while (helper) {
            if (n > 0) {
                counter++;
                n -= 1;
                if (counter % m == 0)
                    n++;
            } else {
                helper = false;
            }

        }

        System.out.println(counter);
    }
}
