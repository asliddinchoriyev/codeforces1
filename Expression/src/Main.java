import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int first = a + b + c;
        int second = a * b * c;
        int third = a + b * c;
        int fourth = a  * b + c;
        int fifth = a * (b + c);
        int sixth = (a + b) * c;

        System.out.println(Math.max(first,Math.max(second,Math.max(third,Math.max(fourth,Math.max(fifth,sixth))))));

    }
}
