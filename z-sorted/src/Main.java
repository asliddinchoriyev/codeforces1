import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        Arrays.sort(array);
        int[] cloneArray = new int[size];
        for (int i = 0; i < size; i++){
            cloneArray[i] = array[size - (i+1)];
        }
        ArrayList<Integer> result = new ArrayList<>();
        int i = 0;
        while (i < (double) size / 2) {
            result.add(array[i]);
            result.add(cloneArray[i]);
            i++;
        }
        if (size % 2 != 0) result.remove(size);

        for (int x : result) {
            System.out.print(x + " ");
        }

    }
}