import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int size = scanner.nextInt();
        int abilityDays[] = new int[7];

        for (int i = 0; i < 7; i++) {
            abilityDays[i] = scanner.nextInt();
        }

        int sum = 0;
        int i = 0;

        while (sum < size) {

            if (i > 7) {
                i = i % 7;
            }
            sum += abilityDays[i % 7];

            i++;
        }

        if(i % 7 == 0){
            System.out.println(7);
        }else {
            System.out.println(i % 7);
        }

    }


}
