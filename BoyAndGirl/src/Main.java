import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int len = str.length();

        int counter = len;
        for (int i = 0; i < len ; i++) {
            for (int j = i+1; j < len; j++) {
                if(str.charAt(i) == str.charAt(j))
                   counter--;
            }
        }


        if(counter % 2 != 0)
            System.out.println("IGNORE HIM!");
        else
            System.out.println("CHAT WITH HER!");
    }
}
