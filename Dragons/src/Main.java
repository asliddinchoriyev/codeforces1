import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        int n = scanner.nextInt();

        int dragons[][] = new int[n - 1][n - 1];
        for (int i = 0; i < n; i++) {

            for (int j = 0; j < i; j++) {
                dragons[i][j] = scanner.nextInt();
            }


        }
    }

    public static int[][] sort(int[][] dragons) {
        Arrays.sort(dragons, new Comparator<int[]>() {
            public int compare(int[] o1, int[] o2) {
                return (Integer.valueOf(o1[0]).compareTo(o2[0]));
            }
        });
        return dragons;
    }
}
