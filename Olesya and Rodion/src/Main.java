import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Long n = scanner.nextLong();
        Long t = scanner.nextLong();
        String s = "";

        if (n == 1 && t >= 10) {
            System.out.println(-1);
        } else if (n >= 2 && t == 10) {
            for (int i = 1; i < n; i++) {
                s += "1";
            }
            s += "0";
            System.out.println(s);
        } else {
            for (int i = 0; i < n; i++) {
                s += t;
            }

            System.out.println(s);
        }


    }
}
