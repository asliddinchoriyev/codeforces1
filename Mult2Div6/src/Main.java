import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cases = scanner.nextInt();

        for (int i = 0; i < cases; i++) {
            int n = scanner.nextInt();

            int counter = 0;
            while (n % 6 == 0) {
                n /= 6;
                counter++;
            }

            while (n % 3 == 0) {
                n /= 3;
                counter += 2;
            }

            if (n == 1)
                System.out.println(counter);
            else
                System.out.println(-1);
        }
    }
}


/////AH Tonmoy
//
/////Department of CSE,23rd batch
//
/////Islamic University,Bangladesh
//
//#include<iostream>
//
//    using namespace std;
//
//    int main()
//
//    {
//
//        int n,c,t;
//
//        cin>>t;
//
//        while(t--)
//
//        {
//
//            c=0;
//
//            cin>>n;
//
//            while(n%6==0)
//
//            {
//
//                n=n/6;
//
//                c++;
//
//            }
//
//            while(n%3==0)
//
//            {
//
//                n=n/3;
//
//                c=c+2;
//
//            }
//
//            if(n==1)
//
//                cout<<c<<endl;
//
//            else
//
//                cout<<"-1"<<endl;
//
//        }
//
//    }