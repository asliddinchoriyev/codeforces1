import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int allTypes = scanner.nextInt();
        int list = scanner.nextInt();

        int allPrice[] = new int[allTypes];

        ArrayList<Integer> prices = new ArrayList<>();
        for (int i = 0; i < allTypes; i++) {
            allPrice[i] = scanner.nextInt();
        }

        for (int i = 0; i < allTypes; i++) {
            prices.add(allPrice[i]);
        }

        String listArray[] = new String[list];
        for (int i = 0; i < list; i++) {
            listArray[i] = scanner.nextLine();

        }
        System.out.println();
        Collections.sort(prices);


        int min = 0;
        for (int i = 0; i < list ; i++) {
            min += prices.get(i);
        }

        int max = 0;
        for (int i = allTypes; i > allTypes - list; i--) {
            max += prices.get(i - 1);
        }

        System.out.println(min + " " + max);


    }
}
