import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int len = str.length();

        String ans = "";
        ArrayList<Character> digits = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            if(Character.isDigit(str.charAt(i))){
                digits.add(str.charAt(i));
            }
        }

        Set set = new Set(digits);

        for (Object o : set) {
            System.out.println(o);
        }

        System.out.println(ans);
    }
}
