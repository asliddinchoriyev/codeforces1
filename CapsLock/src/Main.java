import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        boolean isUp = true;
        boolean isFirstLittleTheRestBig = true;

        for (int i = 0; i <line.length() ; i++) {
            if(!Character.isUpperCase(line.charAt(i))){
                isUp = false;
                break;
            }
        }

        for (int i = 1; i <line.length(); i++) {
            if(Character.isLowerCase(line.charAt(0))){
                if(!Character.isUpperCase(line.charAt(i))){
                    isFirstLittleTheRestBig= false;
                    break;
                }
            }else {
                isFirstLittleTheRestBig = false;
                break;
            }
        }

        if(isUp){
            System.out.println(line.toLowerCase());
        }else if(isFirstLittleTheRestBig){
            System.out.println(line.substring(0,1).toUpperCase() + line.substring(1).toLowerCase());
        }else {
            System.out.println(line);
        }
    }
}
