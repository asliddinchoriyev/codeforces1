import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine().toLowerCase();
        String newLine = "";

        for (int i = 0; i < line.length(); i++) {
         if(line.charAt(i) != 'a' && line.charAt(i) != 'o' &&
                 line.charAt(i) != 'y' && line.charAt(i) != 'u' &&
                 line.charAt(i) != 'e' && line.charAt(i) != 'i'){
             newLine += "." + line.charAt(i);
         }
        }
        System.out.println(newLine);
    }
}
