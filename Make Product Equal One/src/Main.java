import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();

        long array[] = new long[t];

        for (int i = 0; i < t; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println(minStepCounter(array, t));

    }

    public static long minStepCounter(long array[], int n) {
        int zero = 0, negative = 0;
        long step = 0;
        for (int i = 0; i < n; i++) {
            if (array[i] == 0) {
                zero++;
            } else if (array[i] < 0) {
                negative++;

                step += -1 - array[i];

            } else {
                step += array[i] - 1;
            }
        }

        if (negative % 2 == 0) {
            step += zero;
        } else {
            if (zero > 0) {
                step += zero;
            } else {
                step += 2;
            }

        }

        return step;
    }
}
