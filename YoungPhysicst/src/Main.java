import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int cases = scanner.nextInt();

        int x, y, z;
        int sumX = 0;
        int sumY = 0;
        int sumZ = 0;
        for (int i = 0; i < cases; i++) {
            x = scanner.nextInt();
            y = scanner.nextInt();
            z = scanner.nextInt();

            sumX += x;
            sumY += y;
            sumZ += z;
        }

        if(sumX == 0 && sumY == 0 && sumZ == 0)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
