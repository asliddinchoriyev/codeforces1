import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cases = scanner.nextInt();

        for (int i = 0; i < cases; i++) {
            long n = scanner.nextLong();

            while (!checkFair(n)) {
               n++;
            }

            System.out.println(n);
        }
    }


    public static boolean checkFair(long n) {
        long k = n;

        while (k > 0){
            if (k % 10 != 0 && (n % (k % 10) != 0)) {
                return false;
            }
            k = k / 10;
        }
        return true;
    }

}

//import java.util.Scanner;
//
//public class Main {;
//
//    public static void main(String[] args) {
//
//        Scanner scanner = new Scanner(System.in);
//
//        int testCase = scanner.nextInt();
//
//        for(int i = 0; i < testCase; i++) {
//
//            long n = scanner.nextLong();
//
//            while(!solve(n)) {
//                n++;
//            }
//            System.out.println(n);
//        }
//    }
//
//    public static boolean solve(long num) {
//
//        long n = num;
//
//        while(n > 0) {
//
//            if(n % 10 != 0 && num % (n % 10) != 0) {
//                return false;
//            }
//            n = n / 10;
//        }
//        return true;
//    }
//}