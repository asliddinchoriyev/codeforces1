import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int tests = scanner.nextInt();

        for (int i = 0; i < tests ; i++) {

            int number = scanner.nextInt();
            if(solve(number))
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
    public static boolean solve(int n){
        boolean ans = false;

        int butun = n / 2020;
        int qoldiq = n % 2020;

        if(butun >= qoldiq){
            ans  = true;
        }
        return ans;
    }
}
