import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        int array[][] = new int[size][size];
        double randomNumber = Math.round(Math.random() * 100);


        int sumHorizontal = 0;
        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                randomNumber = Math.round(Math.random() * 100);
                int intRandomNumber = (int) randomNumber;
                array[i][k] = intRandomNumber;
                sumHorizontal += array[i][k];
            }
        }


        int sumVertical = 0;
        for (int j = 0; j < size; j++) {
            for (int m = 0; m < size; m++) {
                randomNumber = Math.round(Math.random() * 100);
                int intRandomNumber = (int) randomNumber;
                array[m][j] = intRandomNumber;
                if(isPrime(array[m][j]))
                   sumVertical += array[m][j];
            }
        }




        if (!isPrime(sumHorizontal) && !isPrime(sumVertical)) {
            for (int x = 0; x < size; x++) {
                for (int k = 0; k < size; k++) {
                    if (isPrime(array[x][k]))
                        System.out.print(array[x][k] + " ");
                }
                System.out.println();
            }
        }

    }

    public static boolean isPrime(int n) {
        boolean isPrime = true;

        for (int i = 2; i * i < n; i++) {
            for (int j = 2; j < i; j++) {
                if (i % j == 0)
                    isPrime = false;
            }
        }

        return isPrime;
    }
}
