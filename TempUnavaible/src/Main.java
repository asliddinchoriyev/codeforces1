import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int cases = scanner.nextInt();

        for (int i = 0; i < cases; i++) {

            int startingPoint = scanner.nextInt();
            int finishingPoint = scanner.nextInt();

            int stationPoint = scanner.nextInt();
            int radius = scanner.nextInt();

            int unAvaibleTime = 0;


            if (stationPoint >= startingPoint && stationPoint <= finishingPoint) {
                if (startingPoint <= stationPoint - radius)
                    unAvaibleTime += (stationPoint - radius) - startingPoint;
                if (stationPoint + radius < finishingPoint)
                    unAvaibleTime += finishingPoint - (stationPoint + radius);

            } else if (stationPoint < startingPoint) {
                startingPoint = Math.max(startingPoint, stationPoint + radius);
                unAvaibleTime = Math.max(0, finishingPoint - startingPoint);
            } else {

                finishingPoint = Math.min(finishingPoint, stationPoint - radius);
                unAvaibleTime = Math.max(0, finishingPoint - startingPoint);
            }


            System.out.println(unAvaibleTime);
        }

    }
}
