import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        int games = scanner1.nextInt();

        Scanner scanner2 = new Scanner(System.in);
        String s = scanner2.nextLine();

        int countA = 0;

        for (int i = 0; i < games; i++) {
            if(s.charAt(i) == 'A'){
                countA++;
            }
        }

        if(countA > games - countA) System.out.println("Anton");
        else if(countA == games / 2) System.out.println("Friendship");
        else System.out.println("Danik");

    }
}

