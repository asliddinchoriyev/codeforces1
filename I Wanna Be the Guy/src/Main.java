import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Set<Integer> hashSet = new TreeSet<>();

        int p = scanner.nextInt();
        int arr1[] = new int[p];
        for (int i = 0; i < p; i++) {
            arr1[i] = scanner.nextInt();
        }

        int q = scanner.nextInt();
        int arr2[] = new int[q];
        for (int i = 0; i < q; i++) {
            arr2[i] = scanner.nextInt();
        }

        for (int i : arr1) {
           hashSet.add(i) ;
        }

        for (int i : arr2) {
            hashSet.add(i);
        }

        if(hashSet.size() == n){
            System.out.println("I become the guy.");
        }else {
            System.out.println("Oh, my keyboard!");
        }

    }
}
