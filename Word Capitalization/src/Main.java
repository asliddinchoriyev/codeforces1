import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int games = scanner.nextInt();
        String s = scanner.nextLine();

        int countA = 0;
        int countD = 0;
        for (int i = 0; i < games; i++) {
            if(s.charAt(i) == 'A'){
                countA++;
            }

            if(s.charAt(i) == 'D'){
                countD++;
            }
        }

        if(countA > countD) System.out.println("Anton");
        else if(countA == countD) System.out.println("Friendship");
        else System.out.println("Danik");
        
    }
}
