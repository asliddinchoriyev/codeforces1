import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cases = scanner.nextInt();

        for (int i = 0; i < cases; i++) {
            int n = scanner.nextInt();

            if(n == 1){
                System.out.println("-1");
            }

            if(n != 1){
                String str = "2";
                char chars[] = new char[n - 1];
                Arrays.fill(chars,'3');

                str = str + new String(chars);

                System.out.println(str);
            }


        }
    }
}
