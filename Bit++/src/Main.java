import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        ArrayList<String> list= new ArrayList();
        for (int i = 0; i < n ; i++) {
            list.add(scanner.next());
        }


        int ans = 0;
        for (Object o : list) {
            if(o.equals("X++") || o.equals("++X")){
                ans++;
            }else {
                ans--;
            }
        }

        System.out.println(ans);
    }

}
