import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cases = scanner.nextInt();

        Long numbers[] = new Long[cases];

        for (int i = 0; i < cases; i++) {
            numbers[i] = scanner.nextLong();
        }

        for (int i = 0; i < cases ; i++) {
           solve(numbers[i]);
        }

    }

    public static void solve(Long num) {
        boolean ans = false;
        if (num % 2 != 0) {
            System.out.println("YES");
        } else {
            for (long i = 3L; (i < num && !ans); i += 2) {
                num /= 2;
                if(num % 2 != 0)
                    ans = true;
            }
        }
        if(ans == true)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
