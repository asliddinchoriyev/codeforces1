import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = scanner.nextInt();
        int array[] = new int[problems];

        for (int i = 0; i < problems; i++) {
            array[i] = scanner.nextInt();
        }

        int first = 0;
        int second = 0;
        int third = 0;
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                first = i + 1;
            }
        }

        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                third = i + 1;
            }
        }

        int middle = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[first - 1] && array[i] < array[third - 1]) {
                second = i + 1;
            }
        }

        System.out.println(first + " " + second + " " + third);
    }
}
