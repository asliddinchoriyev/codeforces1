import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner= new Scanner(System.in);
        int n = scanner.nextInt();

        int array[] = {4,7,47,74,44,444,447,474,477,777,774,744};
        boolean res = false;
        for (int i = 0; i < array.length ; i++) {
            if(n % array[i] == 0){
                res = true;
                break;
            }
        }

        if(res)
            System.out.println("YES");
        else
            System.out.println("NO");

    }
}
