import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int days = scanner.nextInt();

        int min,max;
        if(days % 7 == 6){
            min = (days / 7) * 2 + 1;
        }else{
            min = (days / 7) * 2;
        }

        if(days % 7 >= 2){
            max = (days / 7) * 2 + 2;
        }else if(days % 7 == 1){
            max = (days / 7) * 2 + 1;
        }else{
            max = (days / 7) * 2;
        }

        System.out.println(min + " " + max);
    }
}
