import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int casess = scanner.nextInt();

        for (int i = 0; i < casess; i++) {
            System.out.println(solve(scanner.nextInt()));
        }
        System.out.println();
    }

    public static int solve(int n) {
        int cnt = 0;
        for (int i = 1; i <= 9; i++) {
            int s = 0;
            for (int j = 0; j < 9; j++) {
                s = s * 10 + i;
                if (s > n) break;
                cnt++;
            }
        }
        return cnt;
    }
}
