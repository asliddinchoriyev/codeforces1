import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

//        double a = 3;
//        double b = 3;
//        double n = 2;
//
//        double resultDouble = (a * b) / n;
//        int resultInt= (int)resultDouble;
//
//        System.out.println(resultInt);
//        System.out.println(resultDouble);

        System.out.println("How many times do you want to check?");
        int times = scanner.nextInt();

        for(int i = 0; i < times; i++){
            System.out.println("WIDTH = ");
            int width = scanner.nextInt();

            System.out.println("HEIGHT = ");
            int height = scanner.nextInt();

            System.out.println("Number of Friends = ");
            int number = scanner.nextInt();

            if(isPossible(width,height,number)){
                System.out.println("YES");
            }else{
                System.out.println("NO");
            }
        }
    }
    public static boolean isPossible(int w,int h,int n){
       int counter1 = 0;
       int counter2 = 0;

       while (w % 2 == 0){
           w /= 2;
           counter1++;
       }

       while (h % 2 == 0){
           h /= 2;
           counter2++;
       }

       if(Math.pow(2,counter1+counter2) >= n){
           return true;
       }

       return  false;
    }
}
