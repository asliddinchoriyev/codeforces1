import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        String answer = "";

        for (int i = 0; i < n - 1; i++) {

            if(i % 2 != 0)
                answer += "I love that ";
            else
                answer += "I hate that ";
        }

        if(n % 2 == 0)
            answer += "I love it";
        else
            answer += "I hate it";

        System.out.println(answer);
    }
}
