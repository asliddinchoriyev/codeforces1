import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long m = scanner.nextInt();
        long n = scanner.nextInt();
        long a = scanner.nextInt();

       long countM = m / a;
       long countN = n / a;

       if(m % a != 0)
           ++countM;

        if(n % a != 0)
            ++countN;

        System.out.println(countM * countN);
    }
}
