create function add_role(i_id integer, i_name character varying) returns boolean
    language plpgsql
as
$$
begin
    insert into role(id, name) VALUES (i_id, i_name);
    return true;
end;
$$;

alter function add_role(integer, varchar) owner to "postgres";

