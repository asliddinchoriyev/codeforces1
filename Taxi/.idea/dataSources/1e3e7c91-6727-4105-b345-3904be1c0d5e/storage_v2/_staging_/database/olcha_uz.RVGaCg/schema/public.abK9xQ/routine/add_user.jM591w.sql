create or replace function add_user(i_name character varying, i_phone_number character varying, i_email character varying default null, i_age integer, i_password character varying, i_attachment_id integer DEFAULT NULL::integer) returns boolean
    language plpgsql
as
$$
declare


begin

    if i_attachment_id is not null then
        insert into "user" (name, phone_number, email, age, password, attachment_id)
        values (i_name, i_phone_number, i_email, i_age, i_password, i_attachment_id);
    else
        insert into "user" (name, phone_number, email, age, password)
        values (i_name, i_phone_number, i_email, i_age, i_password);

    end if;
    return true;
end;

$$;

alter function add_user(varchar, varchar, varchar, integer, varchar, integer) owner to postgres;

