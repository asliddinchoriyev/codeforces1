import java.util.ArrayList;
import java.util.Scanner;

public class test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        ArrayList<Integer> groups = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            groups.add(scanner.nextInt());
        }

        for (int i = 0; i < n; i++) {
            if (groups.get(i) == 4) {
                groups.remove(groups.get(i));
            }
        }

        for (Integer group : groups) {
            System.out.println(group);
        }
    }
}
