import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        ArrayList<Integer> groups = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            groups.add(scanner.nextInt());
        }

        int counter = 0;

        int len = groups.size();

        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i) == 4) {
                groups.remove(groups.get(i));
                counter++;
            }

            if(groups.get(i) == 1 || groups.get(i) == 2 || groups.get(i) == 3){
                for (int j = 0; j < len; j++) {
                    if(groups.get(i) + groups.get(j) == 4){
                        groups.remove(i);
                        groups.remove(j);
                        counter++;
                    }
                }
            }
        }

            System.out.println(counter);

        }
    }


//import java.util.Arrays;
//import java.util.Scanner;
//
//public class Main {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//
//        int n = scanner.nextInt();
//        int array[] = new int[n];
//
//        for (int i = 0; i < n; i++) {
//            array[i] = scanner.nextInt();
//        }
//        Arrays.sort(array);
//
//        int l = 0, r = n - 1, counter = 0;
//        while (l <= r) {
//            int rest = 4;
//            rest -= array[r];
//            r--;
//            while (l <= r && rest >= array[l]) {
//                rest -= array[l];
//                l++;
//            }
//            counter++;
//        }
//        System.out.println(counter);
//    }
//}