import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();

        for (int i = 0; i < t; i++) {
            int a1 = scanner.nextInt();
            int a2 = scanner.nextInt();
            int b1 = scanner.nextInt();
            int b2 = scanner.nextInt();

            if (a1 == (a2 + b1) && a1 == b2) {
                System.out.println("Yes");
            } else if (a2 == b1 && a2 == (a1 + b2)) {
                System.out.println("Yes");
            } else if (a1 == b1 && a1 == a2 + b2) {
                System.out.println("Yes");
            } else if (a2 == b2 && a2 == a1 + b1) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
