import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        System.out.println(solve(str));
    }

    public static String solve(String str) {

        int len = str.length();
        if (len < 7)
            return "NO";

        for (int i = 0; i < len - 6; i++) {
            if (str.substring(i, i + 7).equals("0000000") || str.substring(i, i + 7).equals("1111111"))
                return "YES";
        }
        return "NO";
    }
}