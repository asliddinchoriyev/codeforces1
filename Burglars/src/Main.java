import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int capacityOfBag = scanner.nextInt();
        int numberOfContainers = scanner.nextInt();

        int boxes = 0;
        int amountOfMatches = 0;
        ArrayList<Integer> allMatchesList = new ArrayList<>();

        for (int i = 0; i < numberOfContainers; i++) {
            boxes = scanner.nextInt();
            amountOfMatches = scanner.nextInt();

            for (int j = 0; j < boxes; j++) {
                allMatchesList.add(amountOfMatches);
            }
        }
        Collections.sort(allMatchesList);
        int sum = 0;

        for (int i = allMatchesList.size(); i > allMatchesList.size() - capacityOfBag; i--) {
            sum += allMatchesList.get(i - 1);
        }

        System.out.println(sum);
    }
}

