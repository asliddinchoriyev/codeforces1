import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        System.out.println(solve(str));
    }

    public static String solve(String str) {

        for (int i = 0; i < str.length(); i++){
            if (str.charAt(i) == 'H' || str.charAt(i) == 'Q' || str.charAt(i) == '9')
                return "YES";
        }
        return "NO";
    }
}
